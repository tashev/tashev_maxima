﻿namespace HW8;

public class Cleaner3 : MainCleaner
{
    public bool HasWater { get; set;}
    public Cleaner3 (string model, bool hasWater) : base(model)
    {
        HasWater = hasWater;
    }
    
    public new void StartCleaning()
    {
        Console.WriteLine($"Начало уборки c водой {HasWater}");
    }
}