﻿using HW8;

MainCleaner[] cleaners = new MainCleaner[3];

cleaners[0] = new Cleaner1("Zanussi");
cleaners[1] = new Cleaner2("Xiaomi");
cleaners[2] = new Cleaner3("Kerher",true);


for (int i = 0; i < cleaners.Length; i++)
{
    cleaners[i].StartCleaning(); // в классе Cleaners3 через массив не работает сокрытый метод
}


var cleaner3 = new Cleaner3("Xiaomi Z457",false);

cleaner3.StartCleaning(); 

//А понял. его смотреть надо в IDE gitlab онлайн или скачать