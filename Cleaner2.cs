﻿namespace HW8;

public class Cleaner2 : MainCleaner
{
    public Cleaner2(string model) : base(model)
    {
        
    }
    public override void StartCleaning()
    {
        Console.WriteLine($"{Model}: Начало уборки");
    }
}