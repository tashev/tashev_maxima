﻿namespace HW8;



public class MainCleaner
{
    public MainCleaner(string model)
    {
        Model = model;
    }

    public virtual string Model { get; set; }

    public virtual void StartCleaning()
    {
        Console.WriteLine("Начало уборки");
    }
    
    public virtual void StartCleaning(string room)
    {
        Console.WriteLine($"{room}: Начало уборки");
    }
}